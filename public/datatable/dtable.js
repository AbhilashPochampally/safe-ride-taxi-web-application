

var table1 = null;
$(function () { 
  
  $.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
      var min = $('#min').val();
      var max = $('#max').val();
      
      var checkDate = data[4];
      console.log(checkDate);
       // use data for the age column
      // var mon = $("#mon").val();
      // var day = $("#day").val();
      
      var rtn = false;
      if (min == "" && max == "") { rtn = true; }
      else if (min == "" && checkDate <= max) { rtn = true;}
      else if(max == "" && checkDate >= min) {rtn = true;}
      else if (checkDate <= max && checkDate >= min) { rtn = true; }
     
      return rtn;
      
    }
  );
  var table1 = $('#table').DataTable();
  $("#min").on("change", function(){table1.draw();})
  
       $("#max").on("change", function(){table1.draw();})

  // var data=table.buttons.exportData();
  new $.fn.dataTable.Buttons(table1, {
    buttons: [
      {
        extend: 'excel',
        text: 'Download',
        className: 'excelButton',
        title: null,
        exportOptions: {
          modifier: {
            search: 'applied',

          },
          columns: [0, 1, 2, 3, 4]
        }
      }

    ]
  });

  table1.buttons(0, null).container().prependTo(
    table1.table().container()
  );


})