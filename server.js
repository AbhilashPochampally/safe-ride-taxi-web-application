var http = require('http');
var path = require('path');
var express = require('express');
var ejs = require('ejs');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
const flash = require('express-flash');
//var MongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');
var app = express();

var routes = require('./server/routes');


app.locals.pretty = true;
app.set('port', process.env.PORT ||8000);
app.set('views', __dirname + '/app/server/views');

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('stylus').middleware({ src: __dirname + '/app/public/css' }));
app.use(express.static(__dirname + '/public'));

var dbURL= "mongodb://gopi:gopi@ds147659.mlab.com:47659/saferidetaxi";

mongoose.connect(dbURL, { useMongoClient: true });
 mongoose.Promise = global.Promise;
 var db = mongoose.connection;
 db.on('error', console.error.bind(console, 'dbURL connection error:'));

 console.log('before calling routes');
app.use('/', routes);


http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});

