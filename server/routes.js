var user=require('../models/user');
var bookingmodel=require('../models/bookingmode');
var express = require('express')
var router = express.Router();
var md5 = require('md5');
var usernamestore='';

var currentuser;

router.get('/', function (req, res) {
    console.log('in get / ');
    res.render('login',{
        wrongcredentialsmessage:""
    });

});

router.get('/login', function(req,res){
    res.render('login',{
        wrongcredentialsmessage: ""
    });
});
router.post('/login', function(req,res){
    var username= req.body.username;
    var password=req.body.password;
    console.log(username+' '+password);
    user.find({username: username, password: password}, function(err, docs){
        if(err) console.log(err);
        if(docs.length>0){
            usernamestore=username;
            console.log(docs);
            res.render('index1');
        } else{
            res.render('login',{
                wrongcredentialsmessage:'Invalid Credentials'
            });
        }
    });
});
router.get('/logout', function(req,res){
    res.redirect('/');
});
router.get('/signup', function(req,res){
    res.render('signup');
});
router.post('/signup', function(req,res){
    var firstname= req.body.firstname;
    var lastname=req.body.lastname;
    var homeaddress=req.body.homeaddress;
    var homecity=req.body.homecity;
    var mobilenumber=req.body.mobilenumber;
    var email=req.body.email;
    var password=req.body.password;
    var confirmpassword=req.body.confirmpassword;
    var username=req.body.username;
    var newuser;
    if(password===confirmpassword){
         newuser= new user({
            username:username,
            password:password,
            email:email,
            firstname:firstname,
            lastname:lastname, 
            homeaddress: homeaddress,
            homecity:homecity,
            mobilenumber:mobilenumber
        });
    }
    console.log('object has been framed');
    newuser.save(function (err, result) {
    if(err){
        console.log(err);
    } else{
        console.log('record added successfully');
        res.redirect('/login');
    }
    });
});

router.get('/adminlogin', function(req,res){
    console.log('render admin login');
    res.render('adminlogin',{
        wrongcredentialsmessage:""
    });
});

router.post('/adminlogin', function(req,res){
    var username= req.body.username;
    var password=req.body.password;
    console.log(username+' '+password);
    user.find({username: username, password: password}, function(err, docs){
        if(err) console.log(err);
        if(docs.length>0){
            console.log(docs);
            res.redirect('/bookingdetails');
        } else{
            res.render('adminlogin',{
                wrongcredentialsmessage:'Invalid Credentials'
            });
        }
    });
});

router.get('/acceptbooking/:id', function(req,res){
    console.log('post method of accept booking');
    var bookingid=req.params.id;
    console.log(bookingid);
  
        bookingmodel.find({}, function(err, docs){
            if(err){
                console.log(err);
            } else{
                res.render('bookingdetails', {
                    docs: docs
                });
            }
        });
});

router.get('/declinebooking/:id', function(req,res){
    console.log('post method of accept booking');
    var bookingid=req.params.id;
    bookingmodel.remove({_id:bookingid}, function(err,db){
        if(err) throw err;
        else{
            console.log('booking id being removed');
           
                bookingmodel.find({}, function(err, docs){
                    if(err){
                        console.log(err);
                    } else{
                        console.log('redirecting to booking details');
                        // res.render('bookingdetails', {
                        //     docs: docs
                        // });
                        res.redirect('/bookingdetails')
                    }
                });
                
      
        }
    });

});

router.get('/index1', function (req, res) {
    console.log('in get / ');
    res.render('index1');

});
router.get('/index2', function (req, res) {
    console.log('in get / ');
    
    res.render('index2');

});
router.get('/index3', function (req, res) {
    console.log('in get / ');
    
    res.render('index3');

});

router.get('/index4', function (req, res) {
    console.log('in get / ');
     res.render('index4');
});

router.get('/adminindex1', function (req, res) {
    console.log('in get / ');
    res.render('adminindex1');

});
router.get('/adminindex2', function (req, res) {
    console.log('in get / ');
    
    res.render('adminindex2');

});
router.get('/adminindex3', function (req, res) {
    console.log('in get / ');
    
    res.render('adminindex3');

});

router.get('/adminindex3', function (req, res) {
    console.log('in get / ');
    
    res.render('adminindex3');

});

router.get('/adminindex5', function (req, res) {
    console.log('in get / ');
    
    res.render('adminindex5');

});



router.post('/confirmbooking', function(req,res){
    console.log('confirm booking called');
    var name= req.body.name;
    var username=req.body.username;
    var pickuplocation=req.body.pickuplocation;
    var username=req.body.username;
    var destination=req.body.destination;
    var pickupTime=req.body.pickupTime;
    var date=req.body.date;
    var typeOfCar=req.body.Comfort;
    var adults=req.body.adults;
    console.log(adults+'  '+typeOfCar+' '+date);
    var newbooking=new bookingmodel({
    name: name,
    username:username,
    pickuplocation:pickuplocation,
    destination:destination,
    pickupTime:pickupTime,
    typeOfCar:typeOfCar,
    numberOfPassangers:adults,
    date:date
    });
    newbooking.save(function(err, result){
        if(err){
            console.log(err)
        }
        else {
            console.log('new booking added succesful');
            // res.redirect('/bookingdetails');
            bookingmodel.find({username:username}, function(err, docs){
                console.log(docs);
                if(err){
                    console.log(err);
                } else{
                    res.render('userbookingdetails', {
                        docs: docs
                    });
                }
            });
        }
    });
    


});
router.get('/index5', function (req, res) {
    console.log('in get / ');
    
    res.render('index5');

});

router.get('/bookingdetails', function(req,res){
    bookingmodel.find({}, function(err, docs){
        if(err){
            console.log(err);
        } else{
            res.render('bookingdetails', {
                docs: docs
            });
        }
    });
    
});
module.exports=router;