var mongoose = require('mongoose');

var UserSchema=mongoose.Schema({
    username:{
        type: String,
        index: true
    },
    password:{
        type:String
    },
    email:{
        type:String
    },
      firstname:{
        type:String
    },
    lastname:{
        type:String
    }, homeaddress: {
        type:String
    },
    homecity:{
        type: String
    },
    mobilenumber:{
        type: Number
    }
});

module.exports=mongoose.model('user', UserSchema);