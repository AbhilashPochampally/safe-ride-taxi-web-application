var mongoose = require('mongoose');

var BookingSchema=mongoose.Schema({
    name:{
        type: String,
        index: true
    },
    username:{
        type:String
    },
    pickuplocation:{
        type:String
    },
    destination:{
        type: String
    },
    pickupTime:{
        type: String
    },
    typeOfCar:{
        type: String
    },
    numberOfPassangers:{
        type: Number
    },
    date:{
        type: String
    }

});

module.exports=mongoose.model('bookingmodel', BookingSchema);